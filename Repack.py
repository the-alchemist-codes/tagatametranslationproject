from lib.AssetManager import AMGL, AMJP, net
from api.AssetManager import KeyType, DecryptOptions
import msgpack, zlib, re


def Repack(asset, digest):
	loc = FastParseLoc(AMGL[f'Loc/english/Localized{asset.name[5:-10]}'].load_txt())

	data = asset.load()

	dec = net.EncryptionHelper.Decrypt(KeyType.APP, data, digest, DecryptOptions.IsFile)

	msg = msgpack.unpackb(dec, raw=False)

	remsg = msgpack.packb( ApplyLoc(msg,loc))

	renc = net.EncryptionHelper.Encrypt(KeyType.APP, remsg, digest, DecryptOptions.IsFile)

	open(asset.id,'wb').write(zlib.compress(renc))

def ApplyLoc(master, loc):
	for main, tree in master.items():
		if main in loc:
			key = [t for t in ['iname','miname','id'] if t in tree[0]][0]
			for item in tree:
				if item[key] in loc[main]:
					item.update(loc[main][item[key]])
	return master

def FastParseLoc(loc):
	ret = {}
	#SRPG_Unit_UN_V2_XXXX_NAME\tname\r\n
	for line in loc.split('\r\n'):
		line = line[5:]
		try:
			main,res = line.split('_',1)
			key,res = res.rsplit('_',1)
			skey,val = res.split('\t')
			main = main.replace('Param','')
			if main not in ret:
				ret[main] = {}
			if key not in ret[main]:
				ret[main][key] = {}
			ret[main][key][skey.lower()] = val
		except:
			pass
	return ret
# MasterParam
master = AMJP['Data/MasterParamSerialized']

# QuestParam
quest = AMJP['Data/QuestParamSerialized']

Repack(master, net.session.enviroment.MasterDigest)
Repack(quest, net.session.enviroment.QuestDigest)

