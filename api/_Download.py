from Session import Session
from Network import Network
from EncryptionHelper import *
from AssetManager import AssetManager
import msgpack
import json
import os
from _shared import respath, outpath


datafiles = [
		('QuestDropParamSerialized', 'quest_digest'),
		('MasterParamSerialized', 'master_digest'),
		('QuestParamSerialized', 'quest_digest'),
]


def Download():
	session = Session(DeviceID = "251ec8a9-2eae-408c-a900-ef999b7a4f49", SecretKey = "3f573a89-7bed-4dc3-af7c-0c11332202df",
	                  IDFV = "3ca48ec7-2081-4339-9fa5-dd99bb423851", IDFA = "3cc59e0f-60f4-444f-8378-dddbb8735aa5")
	net = Network("alchemist.gu3.jp", session)

	version = open(os.path.join(respath, 'version.txt'),'rt').read()
	ret = net.ReqCheckVersion2(version)
	SaveAsJson('CheckVersion', ret)
	
	am = AssetManager(ret['host_dl'], ret['assets'])
	
	for dfile, digest in datafiles:
		data = am[f'Data/{dfile}'].download()
		data = net.EncryptionHelper.Decrypt(KeyType.APP, data, ret[digest], DecryptOptions.IsFile)
		try:
			dir = msgpack.unpackb(data, raw = False)
		except:
			dir = json.loads(data)
		SaveAsJson(dfile[:-10], dir)


def SaveAsJson(name: str, dir: dict):
	open(os.path.join(outpath, f'{name}.json'), 'wb').write(json.dumps(dir, ensure_ascii = False, indent = '\t').encode('utf8'))


if __name__ == "__main__":
	Download()
