from enum import IntEnum


class EnvironmentFlagBit(IntEnum):
	ENV_FLG_NONE = 0,
	ENV_FLG_DLC_DOWNLOAD_OLD = 1,
	ENV_FLG_FORCE_SERIALIZATION = 2,
	ENV_FLG_FORCE_ENCRYPTION = 4,
	ENV_FLG_FORCE_NO_SERIALIZATION = 8,
	ENV_FLG_FORCE_NO_ENCRYPTION = 16,
	ENV_FLG_RANKMATCH = 32


class Enviroment(object):
	def __init__(self):
		self.NativeBaseUrl = "https://production-alchemist.nativebase.gu3.jp"
		self.AuthApiPrefix = "/gauth"
		self.PurchaseApiPrefix = "/charge"
	
	def UpdateEnviroment(self, chkver2_response: dict):
		for key, value in chkver2_response.items():
			self.SetValue(key, value)
	
	def SetValue(self, key: str, value: str):
		self.ClientErrorApi = None
		self.AuthApiPrefix = "/gauth"
		self.PurchaseApiPrefix = "/charge"
		
		if key == "stat":
			self.Stat = int(value)
		if key == "stat_msg":
			self.StatMsg = value
		if key == "stat_code":
			self.StatCode = value
		if key == "time":
			self.ServerTime = int(value)
		if key == "host_ap":
			self.ServerUrl = self.EndsSlashDelete(value)
		if key == "nativebase_url":
			self.NativeBaseUrl = self.EndsSlashDelete(value)
			if not value:
				self.NativeBaseUrl = self.EndsSlashDelete(
						"https://production-alchemist.nativebase.gu3.jp")
		if key == "logcollection_url":
			self.LogCollectionUrl = self.EndsSlashDelete(value)
		if key == "host_dl":
			self.DLHost = value
		if key == "host_site":
			self.SiteHost = value
		if key == "host_news":
			self.NewsHost = value
		if key == "assets":
			self.Assets = value
		if key == "assets_ex":
			self.AssetsEx = value
		if key == "master_digest":
			self.MasterDigest = value
		if key == "quest_digest":
			self.QuestDigest = value
		if key == "pub":
			self.Pub = value
		if key == "pub_u":
			self.PubU = value
		if key == "use_appguard":
			self.UseAppguard = int(value)
		if key == "env_flg":
			self.EnvironmentFlag = int(value)  # EnvironmentFlagBit(int(value))
			# self.ProcessMsgPackEncryptionEnvFlags(self.EnvironmentFlag)
		if key == "env_flg2":
			self.EnvFlg2 = self.ProcessMsgPackEncryptionEnvFlags2(value)
	
	def ProcessMsgPackEncryptionEnvFlags(self, envFlag: EnvironmentFlagBit):
		# if (envFlag & EnvironmentFlagBit.ENV_FLG_FORCE_NO_SERIALIZATION) == EnvironmentFlagBit.ENV_FLG_FORCE_NO_SERIALIZATION:
		# 	DebugUtility.LogWarning("Forcing serialization off")
		# 	GameUtility.Config_UseSerializedParams.Value = false
		# 	GlobalVars.SelectedSerializeCompressMethod = EncodingTypes.ESerializeCompressMethod.JSON
		# 	GlobalVars.SelectedSerializeCompressMethodWasNodeSet = true
		# if ((envFlag & EnvironmentFlagBit.ENV_FLG_FORCE_NO_ENCRYPTION) == EnvironmentFlagBit.ENV_FLG_FORCE_NO_ENCRYPTION)
		# {
		# 	DebugUtility.LogWarning("Forcing encryption off");
		# 	GameUtility.Config_UseEncryption.Value = false;
		# }
		pass
	
	def ProcessMsgPackEncryptionEnvFlags2(self, value: str) -> str:
		value = int(value, 16)
		mask = (1 << value.bit_length()) - 1
		return '%x' % (value ^ mask)
	
	def EndsSlashDelete(self, value: str) -> str:
		if value and value[-1] == '/':
			return value[:-1]
		else:
			return value
