from enum import IntEnum
import struct
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto.Util import Padding
from PIL import Image, ImageOps
import os
from lib._shared import resPath

APP_Image = os.path.join(resPath, 'wall1.png')
DLC_Image = os.path.join(resPath, 'fggBtn.png')


class DecryptOptions(IntEnum):
	None_ = 0
	IsFile = 1
	ExtraKeySaltAT = 2
	ExtraKeySaltATDI = 3


class KeyType(IntEnum):
	APP = 0
	DLC = 1


class EncryptionHelper():
	def __init__(self, Session):
		self.Session = Session
		
		self.aoba = b''
		self.doba = b''
		self.KeySize = 0x80
		self.BlockSize = 0x80
		self.GetSharedKey(KeyType.APP, None)
	
	def Decrypt(self, keyType: KeyType, data: bytes, keySalt: str, options: DecryptOptions) -> bytes:
		#if data == None or len(data) < 0x10:
		#	raise EOFError('invalid data to decrypt')
		
		IV = self.Hex2Byte(
				keySalt) if options == DecryptOptions.IsFile else data[:0x10]
		
		key = SHA256.SHA256Hash(
				b''.join(self.GetSharedKey(keyType, None)) +
				(b'' if options == DecryptOptions.IsFile else keySalt.encode('ascii')) +
				(b'' if options not in [DecryptOptions.ExtraKeySaltAT, DecryptOptions.ExtraKeySaltATDI] else self.Session.AccessToken.encode('ascii')) +
				(self.Session.DeviceID.encode('ascii') if options ==
				                                          DecryptOptions.ExtraKeySaltATDI else b'')
		).digest()[:self.KeySize // 8]
		
		managed = AES.new(key, iv = IV, mode = AES.MODE_CBC)
		managed.key_size = self.KeySize
		
		return Padding.unpad(managed.decrypt(data[0x10:]), block_size = self.BlockSize // 8, style = 'pkcs7')
	
	def DecryptEmbeddedTutorialMasterData(self, data: bytes) -> bytes:
		return self.Decrypt(KeyType.APP, data, '', DecryptOptions.None_)

	def Encrypt(self, keyType: KeyType, data: bytes, keySalt: str, options = False) -> bytes:
		if data == None or len(data) == 0:
			return data
		if not options:
			options = self.UseWhatExtraKeySalt(keySalt)
		
		IV = self.Hex2Byte(
				keySalt) if options == DecryptOptions.IsFile else data[:0x10]

		key = SHA256.SHA256Hash(
				b''.join(self.GetSharedKey(keyType, None)) +
				(b'' if options == DecryptOptions.IsFile else keySalt.encode('ascii')) +
				(b'' if options not in [DecryptOptions.ExtraKeySaltAT, DecryptOptions.ExtraKeySaltATDI] else self.Session.AccessToken.encode('ascii')) +
				(self.Session.DeviceID.encode('ascii') if options ==
				                                          DecryptOptions.ExtraKeySaltATDI else b'')
		).digest()[:self.KeySize // 8]
		
		managed = AES.new(key, iv = IV, mode = AES.MODE_CBC)
		managed.key_size = self.KeySize

		return IV + managed.encrypt(Padding.pad(data,block_size = self.BlockSize // 8, style = 'pkcs7'))
	
	def UseWhatExtraKeySalt(self, apiName: str) -> DecryptOptions:
		if (apiName in ["/chkver2", "/product", "/playnew"]) or (any([apiPath == apiName[:len(apiPath)] for apiPath in ["/gauth/", "/auth/", "/debug/", "/master/", '/mst/']])):
			return DecryptOptions.None_
		# Session.DefaultSession != null and
		if self.Session.AccessToken and apiName[:len("/login")] == '/login':
			return DecryptOptions.ExtraKeySaltAT
		if self.Session.DeviceId == None or self.Session.AccessToken == None:  # Session.DefaultSession == null or
			return DecryptOptions.None_
		return DecryptOptions.ExtraKeySaltATDI
	
	def GetKeyImageAsset(self, keyType: KeyType) -> Image:
		if keyType == KeyType.APP:
			return ImageOps.flip(Image.open(APP_Image))
		else:
			# idstr = self.Session.EnvFlg2
			# (~uint(EnvFlg2, 16)).ToString('x')
			return ImageOps.flip(Image.open(DLC_Image))
	
	def Hex2Byte(self, hex: str) -> bytes:
		return bytes.fromhex(hex)
	
	def IsUseAPPSharedKey(self, apiName: str) -> bool:
		return apiName == "/chkver2" or any([apiPath == apiName[:len(apiPath)] for apiPath in ["/gauth/", "/master/", "/product", "/achieve/", "/debug/", '/mst/']])
	
	def ReverseBits(self, n: int) -> int:
		num = 0
		for i in range(8):
			num = num * 2 + n % 2
			n = n // 2
		return int(num)
	
	def GetSharedKey(self, keyType: KeyType, useThisImage: Image) -> bytes:
		buffer = ''
		if keyType == KeyType.APP and self.aoba:
			return self.aoba
		
		if keyType == KeyType.DLC and self.doba:
			return self.aoba
			# return Enumerable.ToArray<byte>(Enumerable.Select<OByte, byte>(doba,  < > f__am$cache1));
		
		keyImageAsset = self.GetKeyImageAsset(keyType)
		num = 0
		index = 0
		n = 0
		
		test = 0
		
		numArray = [b''] * 0x10  # new OByte[0x10]
		try:
			y = 0
			while True:
				if y >= keyImageAsset.height:
					if (keyType == KeyType.APP):
						self.aoba = numArray
						return self.aoba
					
					else:
						self.doba = numArray
						return self.doba
				
				x = 0
				while True:
					if x < keyImageAsset.width:
						
						pixel = keyImageAsset.getpixel((x, y))  # Color32 p
						num6 = 0
						while True:
							if num6 < 3:
								num7 = num % 3
								n = n * 2 + pixel[num7] % 2
								num += 1
								
								if num % 8 == 0:
									n = self.ReverseBits(int(n))
									if n != 0:
										numArray[index] = int.to_bytes(
												n, length = 8, byteorder = 'big', signed = True).lstrip(b'\x00')
										index += 1
									else:
										if keyType == KeyType.APP:
											self.aoba = numArray
											buffer = self.aoba
										else:
											self.doba = numArray
											buffer = self.doba
										return buffer
								num6 += 1
								continue
							x += 1
							break
						continue
					y += 1
					break
		except Exception as e:
			raise e
		
		return buffer
