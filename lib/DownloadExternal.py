import urllib
import zlib
import json
import os
import shutil
from lib._shared import resPath, versionPath, DownloadWithBar


def Download_External():
# wytesong's compendium
	try:
		print('wytesong\'s copendium')
		SSID = '1nNmHzEfU3OSt-VlGma4diO4405fmqDLWJ8LiiOsyRmg'
		url = "https://script.google.com/macros/s/AKfycbzMqkHxhLsiMWqtFDmMHzqgT4a1R8yhBAxHN6YRkeN1lotYmsfg/exec?type=array&id="+SSID
		data = urllib.request.urlopen(url).read()
		with open(os.path.join(resPath,'wytesong.json'),'wb') as f:
			f.write(data)
	except:
		print('Failed to download wytesong\'s compendium')

# translation project
	try:
		print('translation project')
		SSID = '15kBDpdWkKo5PTY0KL3UVRRjS2qQ7x04wvVDlfCNxVI8'
		url = "https://script.google.com/macros/s/AKfycbzMqkHxhLsiMWqtFDmMHzqgT4a1R8yhBAxHN6YRkeN1lotYmsfg/exec?type=array&id="+SSID
		data = urllib.request.urlopen(url).read()
		with open(os.path.join(resPath,'translation_project_core.json'),'wb') as f:
			f.write(data)
	except:
		print('Failed to download translation project core')