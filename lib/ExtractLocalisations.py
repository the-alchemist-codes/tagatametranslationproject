from ._shared import resPath, ParseSheet
from .AssetManager import AMGL, AMJP
from .Compendium import compendium
#from GenericSkillDescription.GenerateDescription import GenerateDescriptions
import re, json, zlib, os
from lib.GenericSkillDescription import GenericSkillDescription

reLoc = re.compile(r'(SRPG_.+?Param_)?(.+?)\t(.+?)(\t(.+?))?\r?\n')

DUPLICATE_KANJI_PREFRENCE = ['official','project','compendium','generic']

def AIOLocalisations():
	global db
	##############################################################################################
	####	official part	######################################################################
	##############################################################################################
	#	load gl files
	db = ExtractLocalisations(AMGL, type_key = 'official', lang = 'english')
	#	load jp files
	ExtractLocalisations(AMJP, database = db, type_key = 'kanji')

	#	use global loc on data
	for lname, lvals in db['loc'].items():
		#	check if data loc
		if not (all(['_' in key for key in lvals.keys()])):
			continue

		#	filter official translation
		official = {key: val['official'] for key, val in lvals.items() if 'official' in val}

		#	apply localisation
		for dname, dvals in db['data'].items():
			for key, val in official.items():
				if key in dvals:
					dvals[key]['official'] = val

	###########################################################################################
	####	wytesong's compendium	###########################################################
	###########################################################################################
	for key, val in compendium().items():
		db['data']['MasterParam'][f'{key}']['compendium'] = val

	###########################################################################################
	####	translation project		###########################################################
	###########################################################################################
	project_data = json.loads(open(os.path.join(resPath,'translation_project_core.json'),'rb').read())
	for sheet_key in ['sys','help','nav','quest']:
		for translation in ParseSheet(project_data[sheet_key]):
			if 'english' in translation and translation['english'] not in ['',' ','-','/'] and translation['system'] in db['loc'][sheet_key]:
				db['loc'][sheet_key][translation['system']]['project'] = translation['english']

	###########################################################################################
	####	generic skill desc		###########################################################
	###########################################################################################
	skills  = GenericSkillDescription(AMJP['Data/MasterParamSerialized'].load_master())
	for key,expr in skills.items():
		if f'{key}_EXPR' in db['data']['MasterParam']:
			db['data']['MasterParam'][f'{key}_EXPR']['generic'] = expr

	###########################################################################################
	####	duplicate patcher		###########################################################
	###########################################################################################
	#	collect all kanjis and then adds a translation of them to keys which have that kanji but not translation
	kanji = {}
	#	collect
	for mkey, typ in db.items():
		for filename, data in typ.items():
			for key, translations in data.items():
				#	quick cleanup
				for tkey in  set(translations.keys()):
					if translations[tkey] in ['\r',' ','']:
						del db[mkey][filename][key][tkey]

				if 'kanji' in translations:
					if translations['kanji'] not in kanji:
						kanji[translations['kanji']] = {}
					kanji_dict = kanji[translations['kanji']]
					for tkey, val in translations.items():
						if tkey not in kanji_dict:
							kanji_dict[tkey] = val

	#	apply
	for mkey, typ in db.items():
		for filename, data in typ.items():
			for key, translations in data.items():
				if 'kanji' in translations:
					kanji_dict = kanji[translations['kanji']]
					for key_typ in DUPLICATE_KANJI_PREFRENCE:
						if key_typ in kanji_dict:
							translations['duplicate'] = kanji_dict[key_typ]
							break



	open(os.path.join(resPath,'DB.json'), 'wb').write(json.dumps(db, ensure_ascii = False, indent = '\t').encode('utf8'))
	return db


def ExtractLocalisations(am, database = {'loc': {}, 'data': {}},lang = None, type_key = 'spez'):
	for key in reversed(list(am.assetlist.keys())):
		if key[:4] == 'Loc/' and (not lang or lang in key):
			data = am[key].load_txt()
			fname = key.split('/')[-1]
			if fname not in database['loc']:
				database['loc'][fname] = {}
			db_item = database['loc'][fname]

			for key, val in ParseLoc(data).items():
				if key not in db_item:
					db_item[key] = {}
				db_item[key][type_key] = val

		elif key[:5] == 'Data/':
			if 'Serialized' in key:
				data = am[key].load_master()
				ret = ParseParam(data)
			else:
				try:
					data = am[key].load_json()
					ret = ParseParam(data)
				except json.JSONDecodeError:
					#	normal loc file
					try:
						data = am[key].load_txt()
						ret = ParseLoc(data)
					except:
						continue
				except AttributeError:
					continue

			fname = key.split('/')[-1].replace('Serialized','')
			if fname not in database['data']:
				database['data'][fname] = {}
			db_item = database['data'][fname]

			for key, val in ret.items():
				if key not in db_item:
					db_item[key] = {}
				db_item[key][type_key] = val

	return database


def ParseLoc(data):
	if not data:
		return {}
	return {
		match[2]: match[3]
		for match in reLoc.finditer(data + '\n')  # appended newline as regex last line fix
	}


def ParseParam(master):
	return {
			#	find keys with translation
			#	no _ in name and actual string
			'%s_%s' % (item['iname'], skey.upper()): sval
			for mkey, mlist in master.items()
			#	pre check if valid list
			if type(mlist) == list and type(mlist[0]) == dict and 'iname' in mlist[0]
			for item in mlist
			for skey, sval in item.items()
			if (type(sval) == str and '_' not in sval and sval[-2:] != 'PM' and CheckKanji(sval)) or skey == 'name'
	}


def CheckKanji(string):
	if string == '':
		return True
	for c in string:
		num = ord(c)
		if num > 0x4E00 - 1 and num < 0x9FA5 + 1:  # range for kanji characters
			return True
	return False


if __name__ == '__main__':
	AIOLocalisations()
