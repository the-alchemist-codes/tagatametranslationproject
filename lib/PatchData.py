import os, json
from ._shared import resPath, debugPath
import json, zlib, msgpack
from .AssetManager import AMJP, net
from api.AssetManager import DecryptOptions, KeyType
import zipfile
import msgpack
PREFERENCE = ['official','generic','compendium','duplicate','kanji']
DEBUG = False

PATCH = zipfile.ZipFile('patch.zip','w',compression=zipfile.ZIP_DEFLATED)
BACKUP = zipfile.ZipFile('backup.zip','w',compression=zipfile.ZIP_DEFLATED)
if DEBUG:
	DEBUGZ = zipfile.ZipFile('debug.zip','w',compression=zipfile.ZIP_DEFLATED)

IGNOR = []
IGNOR_MI_KEY = [] #key of item

def Hotfix(db):
	global IGNOR
	db['loc']['sys']['DEFAULT_ROOM_COMMENT'][PREFERENCE[0]] = 'Welc0me to join!'

	# for typ,files in db.items():
	# 	for main, tree in [(m,t) for (m,t) in files.items()]:
	# 		if not tree:
	# 			print(main)
	# 			del db[typ][main]
	# 		for key, trans in [(k,t) for (k,t) in tree.items()]:
	# 			if not trans:
	# 				print(key)
	# 				del db[typ][main][key] 
	IGNOR.append('ChallengeCategory')
	IGNOR_MI_KEY.append('birth')
	return db

def PatchData(db=None):
	db = Hotfix(db)

	assetlist = AMJP.assetlist

	for filename, items in db['loc'].items():
		#continue
		asset_key = f'Loc/japanese/{filename}'
		if asset_key not in assetlist:
			continue
		asset = AMJP[asset_key]
		AddPatchedFile(PATCH, asset, BuildLocFile(items).encode('utf-8-sig'))

	# MasterParams
	for key, digest in [
		('MasterParamSerialized', net.session.enviroment.MasterDigest),
		('QuestParamSerialized', net.session.enviroment.QuestDigest)
		]:
		items = db['data'][key[:-10]]
		asset = AMJP[f'Data/{key}']
		data = asset.load_master()
		if DEBUG:	
			open(asset.name.rsplit('/',1)[1][:-10]+'ori.json','wb').write(json.dumps(data, ensure_ascii=False, indent='\t').encode('utf8'))
		for main, tree in data.items():
			if not main in IGNOR and type(tree) == list and type(tree[0]) == dict and 'iname' in tree[0]:
				for item in tree:
					for key in item:
						if key in IGNOR_MI_KEY:
							continue
						translation_key = '%s_%s'%(item['iname'], key.upper())
						if translation_key in items:
							for typ in PREFERENCE:
								if typ in items[translation_key] and items[translation_key][typ] not in ['',' ']:
									item[key] = items[translation_key][typ]
									break
		# repack
		if DEBUG:
			open(asset.name.rsplit('/',1)[1][:-10]+'mod.json','wb').write(json.dumps(data, ensure_ascii=False, indent='\t').encode('utf8'))
		msg = msgpack.packb(data)
		enc = net.EncryptionHelper.Encrypt(KeyType.APP, msg, digest, DecryptOptions.IsFile)
		AddPatchedFile(PATCH, asset, enc)
	PATCH.close()
	BACKUP.close()
	if DEBUG:
		DEBUGZ.close()

def BuildLocFile(items):
	lines = []
	for key,translation in items.items():
		for typ in PREFERENCE:
			added = False
			if typ in translation and translation[typ] not in ['',' ']:
				lines.append((key,translation[typ]))
				added = True
				break
		if not added:
			lines.append((key,' '))

	text = '\r\n'.join([
			f'{key}\t{SmartLineBreak(val)}'
			for (key,val) in lines
		])
	return text

def SmartLineBreak(line):
	parts = []
	if '<br>' in line or len(line)<51:
		return line
	offset = 49
	while len(line) > offset:
		dot = line[:offset].rfind('.')
		space = line[:offset].rfind(' ')
		length = max(dot,space)
		if not length:
			length = offset
		parts.append(line[:length])
		line = line[length:]
	parts.append(line)
	return '<br>'.join(parts)

def AddPatchedFile(db, asset, byts):
	if DEBUG:
		DEBUGZ.writestr(asset.name, byts)
	db.writestr(asset.id,zlib.compress(byts))
	BACKUP.writestr(asset.id, zlib.compress(asset.load()))

if __name__ == '__main__':
	PatchData()