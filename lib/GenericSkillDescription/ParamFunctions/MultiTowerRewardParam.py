from .MultiTowerRewardItem import MultiTowerRewardItem
def MultiTowerRewardParam(json):
    this={}#MultiTowerRewardParamjson)
    if 'iname' in json:
        this['iname'] = json['iname']
    if 'rewards' in json:
        this['mTowerRewardItems']=[
            MultiTowerRewardItem(reward)
            for reward in json['rewards']
        ]
    return this
