from ._variables import ENUM,TRANSLATION
def ObjectiveParam(json):
    this={}#ObjectiveParamjson)
    if 'iname' in json:
        this['iname'] = json['iname']
    this['objective'] =[]
    if 'objective' in json:
        for objective in json['objective']:
            obj={}
            if 'type' in objective:
                obj['Type'] = convertType(ENUM['EMissionType'][objective['type']], objective['val'] if 'val' in objective else 0)
            if 'val' in objective:
                obj['TypeParam'] = objective['val']
            if 'item' in objective:
                obj['item'] = objective['item']
            if 'num' in objective:
                obj['itemNum'] = objective['num']
            if 'item_type' in objective:
                obj['itemType'] = ENUM['RewardType'][objective['item_type']]
            if 'IsTakeoverProgress' in objective:
                obj['IsTakeoverProgress'] = objective['IsTakeoverProgress']
            this['objective'].append(obj)
    return this

def convertType(mtype,mTypeParam=0):
    ret=""
    if mtype == 'KillAllEnemy':
        ret =  TRANSLATION['BONUS_KILLALL']
    elif mtype == 'NoDeath':
        ret =  TRANSLATION['BONUS_NODEATH']
    elif mtype == 'LimitedTurn':
        ret =  TRANSLATION['BONUS_LIMITEDTURN']
    elif mtype == 'ComboCount':
        ret =  TRANSLATION['BONUS_COMBOCOUNT']
    elif mtype == 'MaxSkillCount':
        ret =   TRANSLATION['BONUS_MAXSKILLCOUNT'] if mTypeParam else TRANSLATION['BONUS_NOSKILL']
    elif mtype == 'MaxItemCount':
        ret =   TRANSLATION['BONUS_MAXITEMCOUNT'] if mTypeParam else TRANSLATION['BONUS_NOITEM']
    elif mtype == 'MaxPartySize':
        ret =  TRANSLATION['BONUS_MAXPARTYSIZE']
    elif mtype == 'LimitedUnitElement':
        ret =  TRANSLATION['BONUS_LIMITELEMENT']
    elif mtype == 'LimitedUnitID':
        ret =  TRANSLATION['BONUS_LIMITUNIT']
    elif mtype == 'NoMercenary':
        ret =  TRANSLATION['BONUS_NOMERCENARY']
    elif mtype == 'Killstreak':
        ret =  TRANSLATION['BONUS_KILLSTREAK']
    elif mtype == 'TotalHealHPMax':
        ret =   TRANSLATION['BONUS_TOTALHEALMAX'] if mTypeParam else TRANSLATION['BONUS_NOHEAL']
    elif mtype == 'TotalHealHPMin':
        ret =  TRANSLATION['BONUS_TOTALHEALMIN']
    elif mtype == 'TotalDamagesTakenMax':
        ret =  TRANSLATION['BONUS_TOTALDAMAGESTAKENMAX']
    elif mtype == 'TotalDamagesTakenMin':
        ret =  TRANSLATION['BONUS_TOTALDAMAGESTAKENMIN']
    elif mtype == 'TotalDamagesMax':
        ret =  TRANSLATION['BONUS_TOTALDAMAGESMAX']
    elif mtype == 'TotalDamagesMin':
        ret =  TRANSLATION['BONUS_TOTALDAMAGESMIN']
    elif mtype == 'LimitedCT':
        ret =  TRANSLATION['BONUS_CTMAX']
    elif mtype == 'LimitedContinue':
        ret =  TRANSLATION['BONUS_CONTINUEMAX'] if mTypeParam else TRANSLATION['BONUS_NOCONTINUE']
    elif mtype == 'NoNpcDeath':
        ret =  TRANSLATION['BONUS_NONPCDEATH']
    elif mtype == 'TargetKillstreak':
        ret =  TRANSLATION['BONUS_TARGETKILLSTREAK']
    elif mtype == 'NoTargetDeath':
        ret =  TRANSLATION['BONUS_NOTARGETDEATH']
    elif mtype == 'BreakObjClashMax':
        ret =  TRANSLATION['BONUS_BREAKOBJCLASHMAX']
    elif mtype == 'BreakObjClashMin':
        ret =  TRANSLATION['BONUS_BREAKOBJCLASHMIN']
    elif mtype == 'WithdrawUnit':
        ret =  TRANSLATION['BONUS_WITHDRAWUNIT']
    elif mtype == 'UseMercenary':
        ret =  TRANSLATION['BONUS_USEMERCENARY']
    elif mtype == 'LimitedUnitID_MainOnly':
        ret =  TRANSLATION['BONUS_LIMITEDUNITID_MAINONLY']
    elif mtype == 'MissionAllCompleteAtOnce':
        ret =  TRANSLATION['BONUS_MISSIONALLCOMPLETEATONCE']
    elif mtype == 'OnlyTargetArtifactType':
        ret =  TRANSLATION['BONUS_ONLYTARGETARTIFACTTYPE']
    elif mtype == 'OnlyTargetArtifactType_MainOnly':
        ret =  TRANSLATION['BONUS_ONLYTARGETARTIFACTTYPE_MAINONLY']
    elif mtype == 'OnlyTargetJobs':
        ret =  TRANSLATION['BONUS_ONLYTARGETJOBS']
    elif mtype == 'OnlyTargetJobs_MainOnly':
        ret =  TRANSLATION['BONUS_ONLYTARGETJOBS_MAINONLY']
    elif mtype == 'OnlyTargetUnitBirthplace':
        ret =  TRANSLATION['BONUS_ONLYTARGETUNITBIRTHPLACE']
    elif mtype == 'OnlyTargetUnitBirthplace_MainOnly':
        ret =  TRANSLATION['BONUS_ONLYTARGETUNITBIRTHPLACE_MAINONLY']
    elif mtype == 'OnlyTargetSex':
        ret =  TRANSLATION['BONUS_ONLYTARGETSEX']
    elif mtype == 'OnlyTargetSex_MainOnly':
        ret =  TRANSLATION['BONUS_ONLYTARGETSEX_MAINONLY']
    elif mtype == 'OnlyHeroUnit':
        ret =  TRANSLATION['BONUS_ONLYHEROUNIT']
    elif mtype == 'OnlyHeroUnit_MainOnly':
        ret =  TRANSLATION['BONUS_ONLYHEROUNIT_MAINONLY']
    elif mtype == 'Finisher':
        ret =  TRANSLATION['BONUS_FINISHER']
    elif mtype == 'TotalGetTreasureCount':
        ret =  TRANSLATION['BONUS_TOTALGETTREASURECOUNT']
    elif mtype == 'KillstreakByUsingTargetItem':
        ret =  TRANSLATION['BONUS_KILLSTREAKBYUSINGTARGETITEM']
    elif mtype == 'KillstreakByUsingTargetSkill':
        ret =  TRANSLATION['BONUS_KILLSTREAKBYUSINGTARGETSKILL']
    elif mtype == 'MaxPartySize_IgnoreFriend':
        ret =  TRANSLATION['BONUS_MAXPARTYSIZE_IGNOREFRIEND']
    elif mtype == 'NoAutoMode':
        ret =  TRANSLATION['BONUS_NOAUTOMODE']
    elif mtype == 'NoDeath_NoContinue':
        ret =  TRANSLATION['BONUS_NODEATH_NOCONTINUE']
    elif mtype == 'OnlyTargetUnits':
        ret =  TRANSLATION['BONUS_ONLYTARGETUNITS']
    elif mtype == 'OnlyTargetUnits_MainOnly':
        ret =  TRANSLATION['BONUS_ONLYTARGETUNITS_MAINONLY']
    elif mtype == 'LimitedTurn_Leader':
        ret =  TRANSLATION['BONUS_LIMITEDTURN_LEADER']
    elif mtype == 'NoDeathTargetNpcUnits':
        ret =  TRANSLATION['BONUS_NODEATHTARGETNPCUNITS']
    elif mtype == 'UseTargetSkill':
        ret =  TRANSLATION['BONUS_USETARGETSKILL']
    elif mtype == 'TotalKillstreakCount':
        ret =  TRANSLATION['BONUS_TOTALKILLSTREAKCOUNT']
    elif mtype == 'TotalGetGemCount_Over':
        ret =  TRANSLATION['BONUS_TOTALGETGEMCOUNT_OVER']
    elif mtype == 'TotalGetGemCount_Less':
        ret =  TRANSLATION['BONUS_TOTALGETGEMCOUNT_LESS']
    elif mtype == 'TeamPartySizeMax_IncMercenary':
        ret =  TRANSLATION['BONUS_TEAMPARTYSIZEMAX_INCMERCENARY']
    elif mtype == 'TeamPartySizeMax_NoMercenary':
        ret =  TRANSLATION['BONUS_TEAMPARTYSIZEMAX_NOMERCENARY']
    elif mtype == 'ChallengeCountMax':
        ret =  TRANSLATION['BONUS_CHALLENGECOUNT_MAX']
    # if mtype == 'DeathCountMax':
    #     return TRANSLATION['BONUS_DEATHCOUNTMAX']
    # if mtype == 'DamageOver':
    #     return TRANSLATION['BONUS_DAMAGE_OVER']
    # if mtype == 'SurviveUnit':
    #     return TRANSLATION['BONUS_SURVIVE_UNIT']
    # if mtype == 'KillTargetEnemy':
    #     return TRANSLATION['BONUS_KILL_TARGET_ENEMY']
    # if mtype == 'TakenDamageLessEqual':
    #     return TRANSLATION['BONUS_TAKEN_DAMAGE_LESS_EQUAL'] if mTypeParam else  TRANSLATION['BONUS_NO_DAMAGE']
    # if mtype == 'TakenDamageGreatorEqual':
    #     return TRANSLATION['BONUS_TAKEN_DAMAGE_GREATOR_EQUAL']
    # if mtype == 'LimitedTurnLessEqualPartyOnly':
    #     return TRANSLATION['BONUS_LIMITED_TURN_LESS_EQUAL_PARTY_ONLY']
    # if mtype == 'LimitedTurnGreatorEqualPartyOnly':
    #     return TRANSLATION['BONUS_LIMITED_TURN_GREATOR_EQUAL_PARTY_ONLY']
    # if mtype == 'KillEnemy':
    #     return TRANSLATION['BONUS_KILL_ENEMY']
    # if mtype == 'BreakObj':
    #     return TRANSLATION['BONUS_BREAK_OBJ']
    # if mtype == 'LimitedTurnLessEqualPartyAndNPC':
    #     return TRANSLATION['BONUS_LIMITED_TURN_LESS_EQUAL_PARTY_AND_NPC']
    # if mtype == 'LimitedTurnGreatorEqualPartyAndNPC':
    #     return TRANSLATION['BONUS_LIMITED_TURN_GREATOR_EQUAL_PARTY_AND_NPC']
    if mTypeParam:
        entries=(mTypeParam+',').split(',')[::-1][1:]
        for entry in entries:
            if len(entry)>2 and entry in TRANSLATION:
                entry=TRANSLATION[entry]
        ret=ret.format(*entries)
    return ret if ret else mtype