from ._variables import ENUM
def CostumTargetParam(json):
	this={}
	if 'units' in json:
		this['units'] = json['units']

	if 'jobs' in json:
		this['jobs'] = json['jobs']

	if 'unit_groups' in json:
		this['unit_groups'] = json['unit_groups']

	if 'job_groups' in json:
		this['job_groups'] = json['job_groups']
	if 'first_job' in json:
		this['first_job'] = json['first_job']
	if 'second_job' in json:
		this['second_job'] = json['second_job']
	if 'third_job' in json:
		this['third_job'] = json['third_job']
	if 'sex' in json:
		this['sex'] = ENUM['ESex'][json['sex']]
	if 'birth_id' in json:
		this['birth_id'] = json['birth_id']
	this['element'] = []
	if 'dark' in json:
		this['element'].append('Dark')
	if 'shine' in json:
		this['element'].append('Light')
	if 'thunder' in json:
		this['element'].append('Thunder')
	if 'wind' in json:
		this['element'].append('Wind')
	if 'water' in json:
		this['element'].append('Water')
	if 'fire' in json:
		this['element'].append('Fire')
	return this

