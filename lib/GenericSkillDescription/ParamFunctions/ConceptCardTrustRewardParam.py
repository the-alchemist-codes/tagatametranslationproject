from ._variables import ENUM
def ConceptCardTrustRewardParam(json):
	this={}#ConceptCardTrustRewardParamjson)
	if 'iname' in json:
		this['iname'] = json['iname']
	if 'rewards' in json:
		this['rewards'] = [
			ConceptCardTrustRewardItemParam(reward)
			for reward in   json['rewards']
		]
	return this

def ConceptCardTrustRewardItemParam(json):
	this={}#ConceptCardTrustRewardItemParamjson)
	if 'reward_type' in json:
		this['reward_type'] = ENUM['eRewardType'][json['reward_type']]
	if 'reward_iname' in json:
		this['iname'] = json['reward_iname']
	if 'reward_num' in json:
		this['reward_num'] = json['reward_num']
	return this
