from .Typ import Passive,Skill,Reaction
from .ParamFunctions import *
from copy import deepcopy

def GenerateDescription(master):
    #   1. convert necessar databases
    #   ability, skill, buff, cond, CostumTarget, UnitGroup
    for tree in ['Ability','Skill','Buff','Cond','UnitGroup']:
        transformation = globals()[globals()['methods'][tree]]
        master[tree] = {
            item['iname']:transformation(item)
            for item in master[tree]
        }

    #   2. generate descriptions
    ret = {
        key:_GenerateDescription(item,master)
        for key,item in master['Skill'].items()
    }
    return ret


def _GenerateDescription(skill,master):
    typ=skill['type'] if 'type' in skill else 'no_type'
    eff=skill['effect_type'] if 'effect_type' in skill else 'no_effect_type'
   
    if typ=='Passive':
       return Passive(skill,master)
    elif typ=='Reaction':
        return Reaction(skill,master)
    else:
        return Skill(skill,master)