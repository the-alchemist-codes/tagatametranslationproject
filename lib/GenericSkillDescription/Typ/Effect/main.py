from .Buff import ConvertBuff
from .Condition import ConvertCondition

ATTACK_TYPE={
    'PhyAttack':'physical',
    'MagAttack':'magical'
}

DMG_TYPE={
    'PhyDamage' :   'physical',
    'MagDamage' :   'magical',
    'TotalDamage':  'any'
}

TARGET_Type={
    "Self"	:	"self",
    "SelfSide"	:	"ally",
    "EnemySide"	:	"enemy",
    "UnitAll"	:	"unit",
    "NotSelf"	:	"not self",
    "GridNoUnit"	:	"tile without unit",
    "ValidGrid"	:	"valid tile",
    "SelfSideNotSelf"	:	"ally (not self)",
}
#weapon->formula
#already displayed by the game:
#cost
#count
#lvcap - upper lv for scaling
#target - self, target,enemy, ....
#timing - used,before, after and so on

#casting
#cast_type  #chant/jump
#cast_speed #clock ticks ini-max

#AC - warrior only?
#"AcFromAbilId": "AB_WOR_UPPER_ARGOS",  #from ability set
#"AcToAbilId": "AB_WOR_UPPER_ARGOS_2",  #to ability set
#"AcTurn": 1,   #for x turns

#dual ma
#CollaboMainId
#CollaboHeight


def main(skill,master,prefix='',postfix=''):
    #return '{pre}{effect}{critical}{type}{jewel}{post}\nrates\nother stuff'
    ret=[]
    eff_type=   skill['effect_type'] if 'effect_type' in skill else 'Attack'
    eff_calc =   skill['effect_calc'] if 'effect_calc' in skill else 'Scale'
    eff_val =   skill['effect_value']['max'] if 'effect_value' in skill else 0
    eff_target='on'
    if not prefix and not postfix:
        prefix='Inflicts'
        postfix='DMG'
        if eff_type=='Heal':
            eff_target='to'
            prefix='Heals'
            if eff_calc=='Scale':
                postfix='MATK HP'
            elif eff_calc=='Fixed':
                postfix='HP'
            else:
                input('Heal?\t%s'%skill)
        elif eff_type=='RateHeal':
            eff_target='of'
            prefix='Heals'
            postfix='of total HP'
        
    #effect value fix
    if eff_calc=='Scale' and eff_type!='RateHeal':
        eff_val+=100


    #HP cost
    #hp_cost    #% of own HP sacrifized, multiplication with rate
    #hp_cost_rate   #allways, 100, can be lowered by passives
    if 'hp_cost' in skill:
        ret.append('Sacrfizes %s%% HP to'%skill['hp_cost'])
        ret.append(prefix[:-1].lower())
    elif 'flags' in skill and 'Suicide'	in skill['flags']:
        ret.append('Suicides to')
        ret.append(prefix[:-1].lower())
    else:
        ret.append(prefix)

    #effect
    #effect_type    #attack/reaction,....
    #effect_calc    #scale/fixed
    #effect_value   #ini/max 
    #effect_dead_rate   #% per dead ally
    #effect_lvrate  # % of target level, only used once, GIant Slayer
    #effect_rate    # chance of effect to trigger, ini/max, allways 100
    #effect_hprate  # negative, dmg ~ how low own HP
    #effect_mprate  # 2 uses, negative, probably like hprate???

    #EffectHitTargetNumRate - only used by black longinus

    #Multi-Hit
    #ComboNum   #number of hits
    #ComboDamageRate    #dmg % per hit
    if 'effect_type' in skill:
        mod='' if 'effect_calc' in skill and skill['effect_calc'] == 'fixed' else "%"
        if 'ComboNum' in skill: #multi-hit
            if 'ComboDamageRate' not in skill:
                skill['ComboDamageRate']=100
            effect='%sx %s%s'%(skill['ComboNum'],round(skill['ComboDamageRate']*(eff_val)/100),mod)
        else:
            effect='%s%s'%((eff_val),mod)
        
        ret.append(effect)

    #IsCritical  #forced critical
    if 'IsCritical' in skill and skill['IsCritical']:
        ret.append('critical')

    #type
    #attack_type #Phy/Mag Attack
    #attack_detail  #Blow/Slash, etc.

    #element_type    #elements (shining)
    #element_value  #useless - only used by ninja skills, ini 0, no max
    if 'attack_type' in skill:
        ret.append(ATTACK_TYPE[skill['attack_type']])

    typ=[]
    if 'attack_detail' in skill:
        typ.append(skill['attack_detail'])
    if 'element_type' in skill:
        typ.append(skill['element_type'])

    #IsJewelAbsorb  #jewel absorb skill
    if 'IsJewelAbsorb' in skill:
        typ.append('Jewel')

    ret.append('-'.join(typ+[postfix]))
    #Jewel dmg
    #JewelDamageType    #scale/calc-dmg
    #JewelDamageValue   #%
    if 'JewelDamageType' in skill:
        ret.append('& %s%% of %s as Jewel-DMG'%(
            skill['JewelDamageValue'] if 'JewelDamageValue' in skill else 100,
            'Jewels' if skill['JewelDamageType']=='scale' else 'DMG'
            ))

    ret=[' '.join(ret)]
    if ret[0]=='%s 100%% %s'%(prefix,postfix):
        ret=[]

    #target
    target=TARGET_Type[(skill['target'] if 'target' in skill else 'UnitAll')]
    if 'select_scope' in skill:
        target='all %s %s'%(
            target.replace('y','ies'),
            '[Mapwide]' if skill['select_scope']=='All' else 'within target area'
        )
    target=' %s %s'%(eff_target,target)
    if ret:
        ret[0]+=target
    else:
        ret.append(target)

    funcs={
        'EffectMod' :    [skill],
        'DefIgnor'  :    [skill],
        'Various'   :    [skill],
        'CTManipulation': [skill],
        'ReactTo'   :    [skill],
        'AOE'       :    [skill],
        'Teleport'  :    [skill],
        'Knockback' :    [skill],
        'BuffAndCond':   [skill,master],
        'Flags'     :    [skill]
    }
    for func,inp in funcs.items():
        cret=False
        cret=globals()[func](*inp)
        if cret:
            ret.append(cret)
    if len(ret)>1 and ret[0][0:3]==' on':
        ret=ret[1:]+[ret[0][1:]]
    return '\n'.join(ret)

def EffectMod(skill):
    ret=[]
    mods={
        'effect_dead_rate'  :   '+%s%% for every dead ally',        #effect_dead_rate   #% per dead ally
        'effect_lvrate'     :   '+%s%% for every lv of the target', #effect_lvrate  # % of target level, only used once, GIant Slayer
        'JumpSpcAtkRate'    :   '+%s%% against Jumping Targets',     #JumpSpcAtkRate #dmg up against jump target
        'ElementSpcAtkRate' :   '+%s%% against weak element',       #ElementSpcAtkRate #increased elemental dmg???
        'effect_hprate'     :   '+%s%% of lost HP',  # negative, dmg ~ how low own HP
        'effect_mprate'     :   '+%s%% of lost Jewels',  # 2 uses, negative, probably like hprate???
    }
    for key,val in mods.items():
        if key in skill:
            ret.append(val%abs(skill[key]))
    #effect_rate    # chance of effect to trigger, ini/max, allways 100
    #EffectHitTargetNumRate - only used by black longinus

    #tokkou - JP tag, probably tag for which strong against
    #tk_rate - overwrites basic tk_rate
    if 'tokkou' in skill:
        ret.append('+%s%% against %ss'%(
            skill['tk_rate'] if 'tk_rate' in skill else 50,#"TokkouDamageRate": 50,
            skill['tokkou']
            ))
    return ', '.join(ret) if ret else ''

def Various(skill):
    ret=[]
    #rate # useless ~ should be skill activation chance ~ allways 100
    #random_hit_rate ~ replaces hit/mis calc 0~100
    if 'random_hit_rate' in skill:
        ret.append('Hit-Rate: %s%%'%skill['random_hit_rate'])
    if 'absorb_damage_rate' in skill:
        ret.append('Abosrbs %s%% of the DMG as HP'%skill['absorb_damage_rate'])
    #absorb_damage_rate ~ % of dmg absorbed as HP/Jewels

    #MaxDamageValue ~max dmg
    if 'MaxDamageValue' in skill:
        ret.append('Max DMG: %s'%skill['MaxDamageValue'])

    #DuplicateCount # stack?
    #TargetEx #allways JumpInc???
    return ', '.join(ret)

def DefIgnor(skill):
    ret=[]
    #side_defrate   #def ignor on side attack 0~100
    if 'side_defrate' in skill:
        ret.append('%s%% from Side'%skill['side_defrate'])
    #back_defrate   #def ignor on backstab 0~100
    if 'back_defrate' in skill:
        ret.append('%s%% from Back'%skill['back_defrate'] )
    #ignore_defense_rate #def ignor 0~100
    if 'ignore_defense_rate' in skill:
        ret.append('%s%%'%skill['ignore_defense_rate'])

    return 'Def-Ignor: %s'%', '.join(ret) if ret else ''

####    WIP
def Teleport(skill):    #will need some fine tuning
    #Teleport
    #TeleportType   #before/after skill
    #TeleportTarget #enemy / not self
    #TeleportHeight #max height
    #TeleportIsMove #teleportation counts as move
    if 'TeleportType' not in skill:
        return ''
    #Teleport: type range tiles on target, Height: max height, counts as move
    srange='%s-%s'%(skill['range_min'],skill['range_max']) if 'range_min' in skill else skill['range_max']

    if skill['TeleportType'] == 'Only':
        return 'can move %s tiles after use'%srange

    return 'Teleport:   {type} teleport {range} tile on {target}, Height: {height}{move}'.format(
        type=skill['TeleportType'],
        range='{area} ({range})'.format(area=skill['select_range'],range=srange) if 'select_range' in skill else srange,
        target=TARGET_Type[skill['TeleportTarget']],
        height=skill['TeleportHeight'],
        move=', counts as move' if 'TeleportIsMove' in skill else ''  
    )

def CTManipulation(skill):
    #ct manipulation
    #control_ct_rate    #rate*value, ini,max, allways 100
    #control_ct_value   #-100~100 for scale,
    #control_ct_calc    #scale, add, if not given, % of total
    if 'control_ct_value' not in skill:
        return ''
        
    return '%s: %s%s CT'%(
        skill['target'],
        skill['control_ct_value']['max'] if 'max' in skill['control_ct_value'] else skill['control_ct_value']['ini'],
        '%%' if 'control_ct_calc' not in skill else '%% of current' if skill['control_ct_calc'] == 'scale' else ''
    )

def BuffAndCond(skill,master):
    #buff & cond
    #self_buff_iname
    #target_buff_iname
    #self_cond_iname
    #target_cond_iname
    ret=[]
    for target in ['self','target']:
        tret=[]
        if '%s_buff_iname'%target in skill:
            tret.append(ConvertBuff(skill['%s_buff_iname'%target],master))
        if '%s_cond_iname'%target in skill:
            tret.append(ConvertCondition(skill['%s_cond_iname'%target],master))
        if tret:
            ret.append('%s: %s'%(target.title(), ', '.join(tret)))
    
    if 'AbsorbAndGive' in skill:     #~ Absorb/Give Stats from Buff
        if skill['AbsorbAndGive']=='Absorb':
            ret.append('absorbs stats from enemy to self')
        else:   #Give
            ret.append('gives own stats to target')

    if 'DuplicateCount' in skill:
        ret.append('%sx stackable'%skill['DuplicateCount'])

    return '\n'.join(ret)

def ReactTo(skill):
    #reaction
    #reaction_damage_type # total dmg/phy dmg - should be reaction only
    #reaction_det_lists #react to dmg types
    #effect_rate    # chance of effect to trigger, ini/max, allways 100
    chance=100
    if 'effect_rate' in skill:
        chance=skill['effect_rate']['max']
    if 'reaction_damage_typ' not in skill:
        return ''
    return '%s to %s%s DMG%s'%(
        'Reacts' if chance==100 else '%s chance to react'%chance,
        DMG_TYPE[skill['reaction_damage_typ']],
        ' %s'%', '.join(key.lower() for key in skill['reaction_det_lists']) if 'reaction_det_lists' in skill else '',
        ', Skills included' if 'flags' in skill and  'AllDamageReaction' in skill['flags'] else ''
    )

def Flags(skill):
    flags={
    	#'ExecuteCutin'	:	'ExecuteCutin',
    	#'ExecuteInBattle'	:	'ExecuteInBattle',
    	'EnableChangeRange'	:	'Enable Change Range',
    	'SelfTargetSelect'	:	'Self Target Select',
    	'PierceAttack'	:	'Pierce Attack',
    	'EnableHeightRangeBonus'	:	'Height Range Bonus',
    	'EnableHeightParamAdjust'	:	'Height Param Adjust',
    	'EnableUnitLockTarget'	:	'Lock Target',
    	'CastBreak'	:	'Cast Break',
    	'JewelAttack'	:	'Jewel Attack',
    	'ForceHit'	:	'Force Hit',
    	#'Suicide'	:	'Suicide', moved to start
    	'FixedDamage'	:	'Fixed Damage',
    	'ForceUnitLock'	:	'Force Lock Target',
    	'AllDamageReaction'	:	'All Damage Reaction',
    	'IgnoreElement'	:	'Ignore Element',
    	'PrevApply'	:	'PrevApply',
    	'JudgeHpOver'	:	'Judge Hp Over',
    	'MhmDamage'	:	'Scales with Enemy Max HP',
    	'AcSelf'	:	'Changes Skillset',
    	'AcReset'	:	'Resets Skillset',
    	'HitTargetNumDiv'	:	'Hit Target Num Div',
    	'NoChargeCalcCT'	:	'No Charge Calc CT',
    	'JumpBreak'	:	'Jump Break',
        'SubActuate': 'Allways Active'
    }
    if 'flags' not in skill:
        return ''
    ret=', '.join([
        val for key,val in flags.items() if key in skill['flags']
    ])
    return 'Flags: %s'%ret if ret else ''


def Knockback(skill):
    #knockback
    #KnockBackRate  #chance
    #KnockBackVal   #number of tiles pushed
    #KnockBackDir   #in which direction
    #KnockBackDs    #rarely used, allways target
    if not 'KnockBackVal' in skill:
        return ''
    #Knockback: %val %Direction, Chance: %
    return 'Knockback:\t{val} tile(s) {direction}{chance}'.format(
        val=skill['KnockBackVal'],
        direction=skill['KnockBackDir'] if 'KnockBackDir' in skill else '',#'away',
        chance=', Chance: %s%%'%skill['KnockBackRate'] if 'KnockBackRate' in skill and skill['KnockBackRate']!=100 else ''   
    )
    

def AOE(skill):
    #range
    #select_range #range style, cross, diamond,....
    #range_min  #min tile distance
    #range_max  #max tile distance

    #effect_height #effect over this height

    #select_scope #scope style, like range
    #scope  #scope range

    #line_type  #direct, curved, stab, ....

    #aoe: cross,diamond,square
    #[R: Diamond(5), H: x, S: Diamond(5)]
    text=[]
    if 'range_max' in skill:
        srange='%s-%s'%(skill['range_min'],skill['range_max']) if 'range_min' in skill else skill['range_max']
        text.append('Range: %s'%(
            '{area} ({range})'.format(area=skill['select_range'],range=srange)
            if 'select_range' in skill
            else srange
        ))
    if 'effect_height' in skill:
        text.append('Height: %s'%skill['effect_height'])
    if 'scope' in skill:
        text.append('Area: %s'%(
            '{area} ({range})'.format(area=skill['select_scope'],range=skill['scope']) 
            if 'select_scope' in skill 
            else skill['scope']
            ))
    if 'line_type' in skill:
        text.append('Line: %s'%skill['line_type'])

    return ('[{text}]'.format(text=', '.join(text))) if text else ''