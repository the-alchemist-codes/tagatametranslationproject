from .Buff import ConvertBuff

FIX={}
def ConvertCondition(cond,master,typ=0):
    #   return:
    #       duration:   x ticks/turns   ~ turn_max, chk_timing
    #       conditions: conditions      ~ conditions
    #       value:                      ~ value_max
    #       chance:                     ~ rate_max
    #       conditions strength:        ~ fix or "v_auto_mp_heal",...
    if type(cond)==str:
        if cond in master['Cond']:
            cond=master['Cond'][cond]
        else:
            return 'Condition not found'
    if 'conditions' not in cond:
        return ''
    #boundary
    duration=''
    if 'turn_max' in cond and cond['chk_timing'] != 'Eternal':
        duration='T: %s'%cond['turn_max']
    
    value=''
    if 'value_max' in cond and cond['value_max'] != 100:
        value='V: %s'%cond['value_max']

    chance=''
    if 'rate_max' in cond and cond['rate_max'] != 100:
        chance='C: %s%%'%cond['rate_max']

    #condition fix
    boundary=[b for b in [duration,value,chance] if b]

    expr = '%s%s'%(', '.join(cond['conditions']),'[%s]'%', '.join(boundary) if boundary else '')
    expr = conditionFix(cond, expr, master)
    return expr



def conditionFix(cond, expr, master):
    def addDetail(expr, string, value):
        pos = expr.find(string)
        if pos != -1:
            return '%s (%s)%s'%(
                expr[:pos+len(string)],
                value,
                expr[pos+len(string):]
            )
        else:
            return expr

    if 'v_poison_rate' in cond:
        expr=addDetail(expr,'Poison','%s%%'%cond['v_poison_rate'])
    if 'v_poison_fix' in cond:
        expr=addDetail(expr,'Poison','%s DMG'%cond['v_poison_fix'])
    if 'v_paralyse_rate' in cond:
        expr=addDetail(expr,'Paralyze','%s%%'%cond['v_paralyse_rate'])
    if 'v_blink_hit' in cond:
        expr=addDetail(expr,'Blind','%s%% hit'%cond['v_blink_hit'])
    if 'v_blink_avo' in cond:
        expr=addDetail(expr,'Blind','%s%% avoid'%cond['v_blink_avo'])
    if 'v_death_count' in cond:
        expr=addDetail(expr,'Death Sentence','%s turns'%cond['v_death_count'])
    if 'v_berserk_atk' in cond:
        expr=addDetail(expr,'Berserk','%s%% PATK'%cond['v_berserk_atk'])
    if 'v_berserk_def' in cond:
        expr=addDetail(expr,'Berserk','%s%% PDEF'%cond['v_berserk_def'])
    if 'v_fast' in cond:
        expr=addDetail(expr,'Quicken','%s%%'%cond['v_fast'])
    if 'v_slow' in cond:
        expr=addDetail(expr,'Delay','%s%%'%cond['v_slow'])
    if 'v_donmov' in cond:
        expr=addDetail(expr,'Bind','%s tiles'%cond['v_donmov'])
    if 'v_auto_hp_heal' in cond:
        expr=addDetail(expr,'Auto Heal','%s%%'%cond['v_auto_hp_heal'])
    if 'v_auto_mp_heal' in cond:
        expr=addDetail(expr,'Jewel Auto Charge','%s%%'%cond['v_auto_mp_heal'])
    if 'v_auto_hp_heal_fix' in cond:
        expr=addDetail(expr,'Auto Heal','%s HP'%cond['v_auto_hp_heal_fix'])
    if 'v_auto_mp_heal_fix' in cond:
        expr=addDetail(expr,'Jewel Auto Charge','%s Jewels'%cond['v_auto_mp_heal_fix'])
    if 'BuffIds' in cond:
        for buff in cond['BuffIds']:
            expr+='\n%s'%ConvertBuff(buff, master)
    return expr

"""
    chk_timing
        "EffectCheckTimings": {
        "0": "ActionStart",
        "1": "Eternal",
        "2": "SkillEnd",
        "3": "AttackEnd",
        "4": "DamageEnd",
        "5": "GutsEnd",
        "6": "ClockCountUp",
        "7": "Moment",
        "8": "ActionEnd",
        "9": "MomentStart",
        "10": "PrevApply"
    },
"""