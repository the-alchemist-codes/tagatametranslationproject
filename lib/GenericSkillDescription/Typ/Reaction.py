from .Skill import Skill

def Reaction(skill,master):
	eff=skill['effect_type']
	if eff=='Attack':
		# "target": "UnitAll",
		# "line_type": "Direct",
		# "select_range": "Diamond",
		# "effect_calc": "Scale",
		# "effect_rate": {
		# 	"ini": 100,
		# 	"max": 100
		# },
		# "effect_value": {
		# 	"ini": 0,
		# 	"max": 0
		# },
		# "target_buff_iname": "BUFF_KAJ_REACTION_01",
		# "target_cond_iname": "CON_BASI_SOL_REACTION",
		# "KnockBackRate": 100,
		# "KnockBackVal": 1,
		# "attack_type": "PhyAttack",
		# "attack_detail": "Slash",
		# "formula"
		return Skill(skill,master)
	elif eff=='Buff':
		#''timing', 'target', 'DuplicateCount', 'flags'
		# 'select_scope', 'effect_height', 'reaction_det_lists', 'select_range', 'range_max', 'range_min', 'scope', 
		# 'control_ct_rate', 'control_ct_value', 'control_ct_calc', 
		# ', 'effect_type', 'reaction_damage_type',  'effect_rate',rate', 'defend_effect',  'ignore_defense_rate', 
		# 'target_buff_iname','target_cond_iname', 'self_buff_iname', 
		# "effect_rate": {
		# 	"ini": 20,
		# 	"max": 50
		# },
		# "control_ct_rate": {
		# 	"ini": 100,
		# 	"max": 100
		# },
		# "control_ct_value": {
		# 	"ini": 200,
		# 	"max": 400
		# },
		# "control_ct_calc": "Add",
		# "target_buff_iname": "BUFF_THI_QUICK_ACTION",
		return Skill(skill,master)
	elif eff=='Defend': #"Chance of defending when receiving Dmg"
		# 'defend_effect', 'type', 'timing', 'target', 'effect_type', 'effect_rate', 
		# 'reaction_damage_type', 'reaction_det_lists', 
		# 'effect_calc', 'effect_value', 'shield_type', 'shield_damage_type', 'shield_value', 
		# 'flags'
		# "effect_calc": "Scale",
		# "effect_rate": {
		# 	"ini": 50,
		# 	"max": 80
		# },
		# "effect_value": {
		# 	"ini": 15,
		# 	"max": 20
		# },
		# "reaction_damage_type": "PhyDamage",
		# "shield_type": "UseCount",
		# "shield_damage_type": "TotalDamage",
		# "shield_value": {
		# 	"ini": 2,
		# 	"max": 2
		# },
		if 'effect_value' not in skill:
			skill['effect_value']={'max':0}
		return 'Blocks {value} {dmg}{chance}'.format(
			value='%s%s'%(skill['effect_value']['max'],	'%' if 'effect_calc' in skill and skill['effect_calc']=='Scale' else ''),
			dmg=skill['reaction_damage_type'].replace('Total','').replace('Damage',' Dmg').replace('Phy','Phys'),
			chance=' [%s%% Chance]'%skill['effect_rate']['max'] if 'effect_rate' in skill and skill['effect_rate']['max']!=100 else ''
			)
	elif eff=='FailCondition': #"Chance of inflicting Poison on attacker when receiving Dmg [Range: 1, Height Range: 2]",
		# "effect_rate": {
		# 	"ini": 5,
		# 	"max": 25
		# },
		# "target_cond_iname": "CON_NIN_DOKUKIRI",
		return Skill(skill,master)
	elif eff=='ReflectDamage':   #"When Dmg is received, absorbs part of Dmg received back as HP with chance of lowering attacker's PDEF for three turns [Range: 2, Height Range: 2]",
		# "effect_calc": "Scale",
		# "effect_rate": {
		# 	"ini": 40,
		# 	"max": 60
		# },
		# "effect_value": {
		# 	"ini": -60,
		# 	"max": -40
		# },
		# "absorb_damage_rate": 100,
		# "target_buff_iname": "BUFF_ACHA_STR_REACTION",
		return Skill(skill,master)
	elif eff=='PerfectAvoid':   #Small chance of completely evading physical attacks
		# "effect_rate": {
		# 	"ini": 5,
		# 	"max": 20
		# },
		return '%s%% chance of evading %s attacks'%(skill['effect_rate']['max'],skill['reaction_damage_type'].replace('Total','').replace('Damage',' ').replace('Phy','physical').replace('Mag','magic'))
	elif eff=='DamageControl':   #Chance of reducing Phys Dmg received
		# "effect_rate": {
		# 	"ini": 30,
		# 	"max": 60
		# },
		return '%s%% chance of reducing %s Dmg received when attacked'%(skill['effect_rate']['max'],skill['reaction_damage_type'].replace('Total','').replace('Damage',' ').replace('Phy','physical').replace('Mag','magic'))
	elif eff=='RateHeal':   #Chance of greatly recovering HP when Dmg is received
		# "effect_calc": "Scale",
		# "effect_rate": {
		# 	"ini": 20,
		# 	"max": 40
		# },
		# "effect_value": {
		# 	"ini": 10,
		# 	"max": 25
		# },
		return Skill(skill,master)
	elif eff=='DisableCondition': #When attacked, cancels all buffs cast on attacker [Range: Unlimited, Height Range: Unlimited]
		# "effect_rate": {
		# 	"ini": 100,
		# 	"max": 100
		# },
		# "target_cond_iname": "CON_DIS_PRIN3_REACTION",
		return Skill(skill,master)
	elif eff=='GemsIncDec':
		# "effect_rate": {
		# 	"ini": 100,
		# 	"max": 100
		# },
		# "effect_value": {
		# 	"ini": 1,
		# 	"max": 20
		# },
		return Skill(skill,master)
	elif eff=='Debuff':
		# "effect_rate": {
		# 	"ini": 30,
		# 	"max": 60
		# },
		# "target_buff_iname": "BUFF_WEA_REACTION",
		return Skill(skill,master)
	elif eff=='Heal':
		# "effect_calc": "Scale",
		# "effect_rate": {
		# 	"ini": 100,
		# 	"max": 100
		# },
		# "effect_value": {
		# 	"ini": 50,
		# 	"max": 50
		# },
		# "target_buff_iname": "BUFF_SYO_REACTION_AGA_01",
		return Skill(skill,master)
	elif eff=='Shield':
		# "effect_type": "Shield",
		# "reaction_damage_type": "TotalDamage",
		# "shield_type": "UseCount",
		# "shield_damage_type": "TotalDamage",
		# "shield_value": {
		# 	"ini": 3,
		# 	"max": 3
		# },
		# "control_damage_rate": {
		# 	"ini": 100,
		# 	"max": 100
		# },
		# "control_damage_value": {
		# 	"ini": -30,
		# 	"max": -65
		# },
		# "control_damage_calc": "Scale"
		return Skill(skill,master)

#other stuff
		# "timing": "FirstReaction",Reaction
		# "target": "EnemySide",Self,NotSelf
		# "select_range": "Diamond",Cross
		# "range_min": 2,
		# "range_max": 5,
		# "effect_height": 2,
		# "DuplicateCount": 3,
		# "flags": [
		# 	"AllDamageReaction"
		# ],
		# reactionDamageType: TotalDamage,MapDamage, phyDamage
		# "reaction_det_lists": [ optional
		# 	"Stab",
		# 	"Jump"
		# ]