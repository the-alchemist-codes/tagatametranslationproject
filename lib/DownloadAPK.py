import os, io
from gplaycli import gplaycli
import http.client
import urllib.request
import zipfile
import unitypack
from unitypack.asset import Asset
from lib._shared import resPath, versionPath, DownloadWithBar


def update_versions(version, key):
	with open(versionPath, 'rt') as f:
		versions = {
				line[:2]: line.split('\t')[-1][:-1]
				for line in f
		}
	versions[version] = key
	with open(versionPath, 'wt') as f:
		f.write('\n'.join([
				f'{ver}\t=\t{key}'
				for ver, key in versions.items()
		]))


def UpdateGlobalVer():
	from .DownloadAPK import DownloadGlobal
	fp = DownloadGlobal()
	z = zipfile.ZipFile(fp)
	for f in z.namelist():
		try:
			asset = Asset.from_file(z.open(f))
			ret = get_networkver(asset)
			if ret:
				print('Global networkver:', ret)
				update_versions('GL', ret)
				break
		except:
			pass


def UpdateJapanVer():
	from .DownloadAPK import DownloadJapan
	fp = DownloadJapan()
	z = zipfile.ZipFile(fp)
	bundle = unitypack.load(z.open('assets/bin/Data/data.unity3d'))
	for asset in bundle.assets:
		if asset.name == 'resources.assets':
			ret = get_networkver(asset)
			if ret:
				print('Japan networkver:', ret)
				update_versions('JP', ret)
				break


def get_networkver(asset):
	for _id, obj in asset.objects.items():
		if obj.type == 'TextAsset':
			data = obj.read()
			if data.name == 'networkver':
				return data._obj['m_Script']
	return None


def DownloadGlobal():
	#GooglePlay_Download_APK(['sg.gumi.alchemistww'], resPath)
	QooApp_Download_APK('sg.gumi.alchemistww', resPath)
	return os.path.join(resPath, 'sg.gumi.alchemistww.apk')


def DownloadJapan():
	QooApp_Download_APK('jp.co.gu3.alchemist', resPath)
	return os.path.join(resPath, 'jp.co.gu3.alchemist.apk')


def GooglePlay_Download_APK(apks, downloader_folder = None):
	cred_path = os.path.join(resPath, 'gplaycli.conf')
	
	token_url = "https://matlink.fr/token/email/gsfid"
	gpc = gplaycli.GPlaycli(config_file = cred_path)
	gpc.device_codename = 'bacon'
	gpc.token_enable = True
	gpc.token_url = token_url
	gpc.retrieve_token(force_new = True)
	success, error = gpc.connect()
	assert error is None
	assert success == True
	gpc.progress_bar = True
	gpc.set_download_folder(os.path.abspath('.') if not downloader_folder else downloader_folder)
	gpc.download(apks if type(apks) == list else [apks])


def QooApp_Download_APK(apk, download_folder = None):
	con = http.client.HTTPSConnection('api.qoo-app.com')
	con.connect()
	con.request("GET", f'/v6/apps/{apk}/download')
	res = con.getresponse()
	con.close()
	download_url = res.headers['Location']
	data = DownloadWithBar(download_url)#urllib.request.urlopen(download_url, timeout = 300).read()  # Download_With_Bar(download_url)  #
	if not download_folder:
		download_folder = os.path.abspath('.')
	open(os.path.join(download_folder, f'{apk}.apk'), 'wb').write(data)


# def Download_With_Bar(url):
# 	resp = urllib.request.urlopen(url)
# 	length = resp.getheader('content-length')
# 	if length:
# 		length = int(length)
# 		blocksize = max(4096, length // 100)
# 	else:
# 		blocksize = 1000000  # just made something up
	
# 	# print(length, blocksize)
	
# 	buf = io.BytesIO()
# 	size = 0
# 	while True:
# 		buf1 = resp.read(blocksize)
# 		if not buf1:
# 			break
# 		buf.write(buf1)
# 		size += len(buf1)
# 		if length:
# 			print(' {:.2f}\r{}'.format(size / length, url), end = '')
# 	buf.seek(0)
# 	return buf.read()
