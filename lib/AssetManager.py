from lib._shared import resPath, versionPath
from api import AssetManager, Network, Session
from lib.DownloadAPK import UpdateGlobalVer, UpdateJapanVer
import os

def LoadVersion():
	global gameVersion_gl, gameVersion_jp
	with open(versionPath, 'rt') as f:
		for line in f:
			ver = line.split('\t')[-1][:-1]
			if line[:2] == 'GL':
				gameVersion_gl = ver
			elif line[:2] == 'JP':
				gameVersion_jp = ver
LoadVersion()


def AssetManager_Init():
	global AMGL, AMJP, net
	print('Global Asset-Manager')
	net = Network("app.alcww.gumi.sg", Session(IsUseEncryption = False))
	try:
		net.ReqCheckVersion2(gameVersion_gl)
	except KeyError:
		print('Local NetworkVer outdated, downloading the latest APK to update it')
		UpdateGlobalVer()
		net.ReqCheckVersion2(gameVersion_gl)
	AMGL = AssetManager(os.path.join(resPath,'global'),net.session.enviroment)
	
	print('Japan Asset-Manager')
	net = Network("alchemist.gu3.jp", Session(IsUseEncryption = True))
	try:
		ret = net.ReqCheckVersion2(gameVersion_jp)
	except KeyError:
		print('Local NetworkVer outdated, downloading the latest APK to update it')
		print('The following warnings are normal.')
		UpdateJapanVer()
		ret = net.ReqCheckVersion2(gameVersion_jp)
	AMJP = AssetManager(os.path.join(resPath,'japan'),net.session.enviroment,'win32')
AssetManager_Init()