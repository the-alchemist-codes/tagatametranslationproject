import json
import os
from ._shared import resPath, ParseSheet
from .AssetManager import AMGL, AMJP

def compendium():
	cDB = json.loads(open(os.path.join(resPath, 'wytesong.json'), 'rb').read())
	dU, masterparam = GameData()
	ret = {}
	cU, cJ = com_unitsNjobs(cDB, dU, masterparam)

	ret.update(cU)
	ret.update(cJ)
	ret.update(com_cards(cDB))
	ret.update(com_masterability(cDB, cU, masterparam))

	return ret

####	master abilities	##########################################################################################
def com_masterability(cDB, units, masterparam):
	#cDB = json.loads(open(os.path.join(resPath, 'wytesong.json'), 'rb').read())
	cM  = ParseSheet(cDB['MasterAbilities'])
	ret = {}
	for masterability in cM:
		for ukey, name in units.items():
			if name in masterability['character'] or name in masterability['keyword']:
				ability = masterparam['Unit'][ukey[:-5]]['ability']
				ret[f'{ability}_NAME'] = masterability['skillname'].split('\n')[0]
				skill   = masterparam['Ability'][ability]['skl1']
				ret[f'{skill}_NAME'] = masterability['skillname'].split('\n')[0]
	return ret

####	nensou				##########################################################################################
def com_cards(cDB):
	#cDB = json.loads(open(os.path.join(resPath, 'wytesong.json'), 'rb').read())
	return {
		card['code']+'_NAME' : card['nameen']
		for card in ParseSheet(cDB['memcardnensou'])
		if 'code' in card and card['code'] and 'nameen' in card
	}

####	units and jobs		##########################################################################################
def com_unitsNjobs(cDB, dU, masterparam):
	#dU, masterparam = GameData()
	cU = CompendiumUnits(cDB)

	match = {}
	matchJ = {}

	def lsCheck(d, c):
		#	checks if all numbers of the leaderskill of the data are also in the possible match ls
		try:
			lsbuff = masterparam['Buff'][masterparam['Skill'][d['leaderskill']]['t_buff']]
			for i in range(10):
				if 'vmax%s' % i in lsbuff:
					if str(abs(lsbuff['vmax%s' % i])) not in c['leaderskill']:
						return False
			return True
		except:
			return False

	def setJobTrans(iname, name):
		if not iname:
			return
		if iname not in matchJ:
			matchJ[iname] = name
		elif matchJ[iname] != name:
			pass

	# print(matchJ[iname], name,cunit['name'])

	def jobDispositionCheck(d, c):
		return True if all([bool(d['jobs'][i]) == bool(c['jobs'][i]) for i in range(6)]) else False

	def jobCheck(d, c):
		for j, job in enumerate(d['jobs']):
			if c['jobs'][j] and job in matchJ and matchJ[job] != c['jobs'][j]:
				if j > 2:  # je/j+
					#	maybe error in the written job, filthy inconsistens
					if matchJ[job].split(':')[0].split('[')[0].replace('\n', '').rstrip(' ') != c['jobs'][j].split(':')[0].split('[')[0].replace('\n', '').rstrip(' '):
						return False
				else:
					return False
		return True

	#	first round - perfect matches
	#	used to get the safe job names for round two
	i = 0
	while i < len(cU):
		cunit = cU[i]
		found = False
		for dunit in dU:
			if all([cunit[key] == dunit[key] for key in ['name jpn', 'element', 'rarity', 'origin']]) and lsCheck(dunit, cunit) and jobDispositionCheck(dunit, cunit):
				found = True
				break
		if found:
			match[dunit['iname']] = cunit
			for j in range(6):
				if cunit['jobs'][j]:
					setJobTrans(dunit['jobs'][j], cunit['jobs'][j])
			dU.remove(dunit)
			cU.remove(cunit)
		else:
			# print(cunit['name'])
			i += 1

	#	2nd round
	match2 = {}
	i = 0
	while i < len(cU):
		cunit = cU[i]
		found = False
		for dunit in dU:
			if all([cunit[key] == dunit[key] for key in ['element', 'rarity', 'origin']]) and lsCheck(dunit, cunit) and jobCheck(dunit, cunit) and jobDispositionCheck(dunit, cunit):
				found = True
				break
		if found:
			match2[dunit['iname']] = cunit
			for j in range(6):
				if cunit['jobs'][j]:
					setJobTrans(dunit['jobs'][j], cunit['jobs'][j])
			dU.remove(dunit)
			cU.remove(cunit)
		else:
			# print(cunit['name'])
			i += 1

	#	3rd round
	match3 = {}
	i = 0
	while i < len(cU):
		cunit = cU[i]
		found = False
		for dunit in dU:
			if all([cunit[key] == dunit[key] for key in ['element', 'rarity', 'origin']]) and jobCheck(dunit, cunit):
				found = True
				break
		if found:
			match3[dunit['iname']] = cunit
			for j in range(6):
				if cunit['jobs'][j]:
					setJobTrans(dunit['jobs'][j], cunit['jobs'][j])
			dU.remove(dunit)
			cU.remove(cunit)
		else:
			# print(cunit['name'])
			i += 1

	#	4th round
	match4 = {}
	i = 0
	while i < len(cU):
		cunit = cU[i]
		found = False
		for dunit in dU:
			if all([cunit[key] == dunit[key] for key in ['element']]) and jobCheck(dunit, cunit):
				found = True
				break
		if found:
			match4[dunit['iname']] = cunit
			for j in range(6):
				if cunit['jobs'][j]:
					setJobTrans(dunit['jobs'][j], cunit['jobs'][j])
			dU.remove(dunit)
			cU.remove(cunit)
		else:
			# print(cunit['name'])
			i += 1

	# SaveJson('match', match)
	# SaveJson('match2', match2)
	# SaveJson('cu', cU)
	# SaveJson('du', dU)
	match.update(match2)
	match.update(match3)
	match.update(match4)
	matchU = {f'{key}_NAME': val['name'] for key, val in match.items()}
	matchJ = {f'{key}_NAME': val for key, val in matchJ.items()}
	return matchU, matchJ


def CompendiumUnits(cDB):
	#cDB = json.loads(open(os.path.join(resPath, 'wytesong.json'), 'rb').read())
	cU = ParseSheet(cDB['Units'])
	origins = [val for key, val in BIRTH.items()]
	#	fix some stuff for better comparision
	j=0
	while j < len(cU):
		unit = cU[j]
		if 'name' not in unit:
			cU.pop(j)
			continue
		j+=1
		#	seperate ls and kaigan ls
		if '**KaiganUP**' in unit['leaderskill']:
			unit['leaderskill'], unit['leaderskillkaigan'] = unit['leaderskill'].split(
					'**KaiganUP**')

		#	seperate jobs names into translation and kanji
		unit.update({'jobs': [None] * 6, 'jobskanji': [None] * 6})
		for i in range(1, 4):
			if 'job%s' % i in unit and unit['job%s' % i] != 'N/A':
				unit['jobs'][i - 1], unit['jobskanji'][i - 1] = unit['job%s' % i].split('\n')
			if 'jc%s' % i in unit and unit['jc%s' % i] != 'N/A':
				unit['jobs'][i + 2], unit['jobskanji'][i + 2] = unit['jc%s' % i].split('\n')
		# keyword to list
		unit['keyword'] = unit['keyword'].split(';')

		# fix birth
		if 'origin' not in unit:
			unit['origin'] = 'ETC'
		if unit['origin'] not in origins:
			sin = False
			#	please slap whoever does the wiki,
			for key, val in BIRTH_SIN.items():
				if key in unit['origin']:
					sin = True
					unit['origin'] = val
			if not sin:
				unit['origin'] = 'ETC'
	return cU


def GameData():
	#	get work data ready
	masterparam = {
			main: {
					item['iname']: item
					for item in tree
			}
			for main, tree in AMJP['Data/MasterParamSerialized'].load_master().items()
			if type(tree) == list and type(tree[0]) == dict and 'iname' in tree[0]
	}
	units = []

	# add j+/je
	for key, js in masterparam['JobSet'].items():
		if 'cjob' in js:
			unit = masterparam['Unit'][js['target_unit']]
			if len(unit['jobsets']) < 6:
				unit['jobsets'].extend([None] * (6 - len(unit['jobsets'])))

			for index, job in enumerate(unit['jobsets']):
				if index > 2:
					continue
				if job == js['cjob']:
					unit['jobsets'][index + 3] = js['iname']

	# load loc
	loc = AMJP['Loc/japanese/unit'].load().decode('utf16')
	import re
	reLoc = re.compile(r'(.+?)\t(.+?)?\r?\n')
	loc_keys = set([match[1].rsplit('_', 1)[0] for match in reLoc.finditer(loc + '\n')])

	for key, unit in masterparam['Unit'].items():
		if key not in loc_keys or not all([key in unit for key in ['birth', 'elem', 'ls6']]) or '_EN_' in key or key[-6:] == '_TRIAL':
			continue
		units.append({
				'iname'      : unit['iname'],
				'name jpn'   : unit['name'],
				'element'    : ELEMENT[unit['elem']],
				'rarity'     : '%s%s' % ('★' * (unit['rare'] + 1), '☆' * (4 - unit['rare'])),
				'jobs'       : [
						masterparam['JobSet'][unit['jobsets'][i]]['job'] if i < len(
								unit['jobsets']) and unit['jobsets'][i] else None
						for i in range(6)
				],
				'origin'     : BIRTH[unit['birth']],
				'leaderskill': unit['ls6'],
				'kaigan'     : True if 'kaigan' in unit else False
		})
	return units, masterparam


ELEMENT = {
		0: 'None',
		1: 'Fire',
		2: 'Water',
		3: 'Wind',
		4: 'Thunder',
		5: 'Light',
		6: 'Dark',
}

BIRTH = {
		"その他"      : "ETC",  # 0&100
		"エンヴィリア"   : "Envylia",  # 1 -
		"ラーストリス"   : "Wrathtris",  # 2 -
		"スロウスシュタイン": "Slothstein",  # 3 -
		"ルストブルグ"   : "Lustoberg",  # 4 -
		"グラトニー＝フォス": "Gluttony Foz",  # 5 -
		"グリードダイク"  : "Greed Dike",  # 6 -
		"サガ地方"     : "Saga",  # 7 -
		"ワダツミ"     : "Wadatsumi",  # 8 -
		"砂漠地帯"     : "Desert",  # 9 -
		"ノーザンブライド" : "Northern Pride",  # 10 -
		"ロストブルー"   : "Lost Blue",  # 11 -
}

BIRTH_SIN = {
		"Envy"    : "Envylia",  # 1 -
		"Wrath"   : "Wrathtris",  # 2 -
		"Sloth"   : "Slothstein",  # 3 -
		"Lust"    : "Lustoberg",  # 4 -
		"Gluttony": "Gluttony Foz",  # 5 -
		"Greed"   : "Greed Dike",  # 6 -
		"Pride"   : "Northern Pride",  # 10 -
}


def SaveJson(name, obj):
	open(name + '.json', 'wb').write(json.dumps(obj, ensure_ascii = False, indent = '\t').encode('utf8'))
